import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Login Page',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String? _savedEmail;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _retrieveEmail();
  }

  Future<void> _retrieveEmail() async {
    final prefs = await SharedPreferences.getInstance();

    if (!prefs.containsKey('email')) {
      return;
    }

    final savedEmail = prefs.getString('email');

    setState(() {
      _savedEmail = savedEmail;
      _emailController.text = savedEmail ?? '';
    });
  }

  Future<void> _saveEmail() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('email', _emailController.text);
    prefs.setString('password', _passwordController.text);
  }

  Future<void> _clearEmail() async {
    final prefs = await SharedPreferences.getInstance();

    if (prefs.containsKey('email')) {
      await prefs.remove('email');
    }

    if (prefs.containsKey('password')) {
      await prefs.remove('password');
    }

    setState(() {
      _savedEmail = null;
      _emailController.clear();
      _passwordController.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login Page'),
      ),
      body:
      Padding(
        padding: const EdgeInsets.all(25),
        child: _savedEmail == null
            ? Column(
          children: [
            Image.asset('assets/logo.png',
              width: 200,
              height: 200,),
            // SizedBox(height: 16),
            TextField(
              controller: _emailController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Email Address',
              ),
            ),
            const SizedBox(height: 15),
            TextField(
              controller: _passwordController,
              obscureText: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: ElevatedButton(
                onPressed: _saveEmail,
                child: const Text('Login'),
              ),
            )
          ],
        )
            : Column(
          children: [
            Image.asset('assets/logo.png',
              width: 200,
              height: 200,),
              SizedBox(height: 16),
            TextField(
              controller: _emailController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Email Address',
              ),
            ),
            const SizedBox(height: 15,),
            TextField(
              controller: _passwordController,
              obscureText: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: ElevatedButton(
                onPressed: _clearEmail,
                child: const Text('Reset'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
